// Very much based on https://github.com/PistonDevelopers/conrod/blob/master/examples/hello_world.rs

#[macro_use]
extern crate conrod;
#[macro_use]
extern crate conrod_derive;
extern crate rand;

mod event_loop;
mod source;
mod sweeper;

use conrod::backend::glium::glium::glutin::{ContextBuilder, Event, EventsLoop, WindowBuilder, WindowEvent};
use conrod::backend::glium::glium::glutin::dpi::LogicalSize;
use conrod::backend::glium::glium::texture::Texture2d;
use conrod::backend::glium::glium::{Display, Surface};
use conrod::backend::glium::Renderer;
use conrod::text::Font;
use conrod::{image, widget, UiBuilder, UiCell, Positionable, Widget};

use event_loop::EventLoop;
use source::Source;

use std::env;
use std::iter::Iterator;
use std::process;

const CHANNEL_NAMES: &[&str] = &["Bass", "Mid-Lo", "Mid-Hi", "Treble"];
const SAMPLE_COUNT: usize = 1000;
const RANGE_Y: (f64, f64) = (0.0, 100.0);

widget_ids! {
    struct Ids {
        matrix,
    }
}

struct State {
    source: Box<dyn Source>,
    current_col: usize,
    channels: Vec<Vec<f64>>,
}

impl State {
    fn new(source: Box<dyn Source>) -> State {
        State {
            source,
            current_col: 0,
            channels: (0..CHANNEL_NAMES.len()).map(|_| (0..SAMPLE_COUNT).map(|_| 0.0).collect()).collect(),
        }
    }
}

fn update_channels(state: &mut State) {
    state.source.new_frame();
    while let Some(channel_updates) = state.source.next() {
        let col = state.current_col;
        state.channels.iter_mut().map(|chan| &mut chan[col]).zip(channel_updates.into_iter()).for_each(|(dest, src)| *dest = src);
        state.current_col += 1;
        state.current_col %= SAMPLE_COUNT;
    }
}

fn render_channel(channel: usize, state: &State, cell: widget::matrix::Element, ui: &mut UiCell) {
    cell.set(
        sweeper::Sweeper::new(
            CHANNEL_NAMES[channel],
            &state.channels[channel],
            state.current_col,
            RANGE_Y.0, RANGE_Y.1),
        ui);
}

fn render(state: &State, ids: &Ids, ui: &mut UiCell) {
    // widget::Text::new("Hello, world!").middle_of(ui.window).color(conrod::color::WHITE).font_size(32).set(ids.text, ui);
    let rows = 2;
    let cols = CHANNEL_NAMES.len() / rows;
    let mut matrix = widget::Matrix::new(cols, rows)
        .cell_padding(5.0, 5.0)
        .middle_of(ui.window)
        .set(ids.matrix, ui);
    while let Some(cell) = matrix.next(ui) {
        render_channel(cell.row * cols + cell.col, state, cell, ui);
    }
}

fn main() {
    let (width, height) = (400.0, 300.0);

    let mut events_loop = EventsLoop::new();
    let mut event_loop = EventLoop::new();

    let window = WindowBuilder::new()
        .with_title("Sweep-O-Inator")
        .with_dimensions(LogicalSize::new(width, height));
    let context = ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(2);
    let display = Display::new(window, context, &events_loop).unwrap();
    let image_map = image::Map::<Texture2d>::new();
    let mut renderer = Renderer::new(&display).unwrap();

    let mut ui = UiBuilder::new([width as f64, height as f64]).build();
    let ids = Ids::new(ui.widget_id_generator());
    // ids.areas.resize(CHANNELS.len(), &mut ui.widget_id_generator());

    let noto_sans = Font::from_bytes(&include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/NotoSans-Regular.ttf"))[..]).unwrap();
    ui.fonts.insert(noto_sans);

    {
        let mut args = env::args().skip(1);
        let source = match args.next().as_ref().map(|arg| arg as &str) {
            Some("stdin") | None =>
                Box::new(source::Tsv::from_stdin()) as Box<Source>,
            Some("random") => Box::new(source::Random::new()),
            Some(arg) => panic!("Invalid source {:?}, must be 'stdin' (default) or 'random'", arg),
        };
        let mut state = State::new(source);

        'main: loop {
            for event in event_loop.next(&mut events_loop) {
                match event.clone() {
                    Event::WindowEvent { event, .. } => match event {
                        WindowEvent::CloseRequested => break 'main,
                        _ => {}
                    },
                    _ => {}
                }

                // Use the `winit` backend feature to convert the winit event to a conrod input.
                let input = match conrod::backend::winit::convert_event(event, &display) {
                    None => continue,
                    Some(input) => input,
                };
                ui.handle_event(input);
            }

            let ui = &mut ui.set_widgets();
            update_channels(&mut state);
            render(&state, &ids, ui);
            // TODO: maybe hook the file I/O into the event loop somehow?

            if let Some(primitives) = ui.draw_if_changed() {
                renderer.fill(&display, primitives, &image_map);
                let mut target = display.draw();
                target.clear_color(1.0, 1.0, 1.0, 1.0);
                renderer.draw(&display, &mut target, &image_map).unwrap();
                target.finish().unwrap();
            }
        }
    }

    // For we get stuck in the event loop when using the Random Source :(
    process::exit(0);
}
