mod random;
mod tsv;

pub use self::random::Random;
pub use self::tsv::Tsv;

pub trait Source {
    fn new_frame(&mut self) {}
    fn next(&mut self) -> Option<Vec<f64>>;
}
