use super::Source;

use std::io::{self, BufRead, Error, ErrorKind};
use std::iter::Iterator;
use std::str::FromStr;
use std::sync;
use std::thread;

fn try_read_f64(value: &str) -> Result<f64, Error> {
    f64::from_str(value).map_err(|err| Error::new(ErrorKind::InvalidData, err))
}

fn line_sender<R: BufRead>(read: &mut R, sender: sync::mpsc::Sender<String>) {
    for line in read.lines() {
        sender.send(line.unwrap()).unwrap();
    }
}

pub struct Tsv {
    line_receiver: sync::mpsc::Receiver<String>,
}

impl Tsv {
    pub fn from_stdin() -> Tsv {
        let (tx, rx) = sync::mpsc::channel();
        thread::spawn(move || {
            let stdin = io::stdin();
            let mut stdin_lock = stdin.lock();
            line_sender(&mut stdin_lock, tx)
        });
        Tsv {
            line_receiver: rx,
        }
    }
}

impl Source for Tsv {
    fn next(&mut self) -> Option<Vec<f64>> {
        self.line_receiver.try_recv()
            .ok()
            .map(|line| line.split('\t').map(try_read_f64).map(|x| x.unwrap()).collect())
    }
}
