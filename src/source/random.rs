use rand::{self, FromEntropy, Rng};
use super::Source;
use CHANNEL_NAMES;
use RANGE_Y;

pub struct Random {
    rng: rand::rngs::SmallRng,
    should_update: bool,
    state: Vec<f64>,
}

impl Random {
    pub fn new() -> Random {
        Random {
            rng: rand::rngs::SmallRng::from_entropy(),
            should_update: true,
            state: (0..CHANNEL_NAMES.len()).map(|_| 0.0).collect(),
        }
    }
}

impl Source for Random {
    fn new_frame(&mut self) {
        self.should_update = true;
    }

    fn next(&mut self) -> Option<Vec<f64>> {
        if self.should_update {
            self.should_update = false;
            for channel in self.state.iter_mut() {
                let target = self.rng.gen_range(RANGE_Y.0, RANGE_Y.1);
                *channel += (target - *channel) / 10.0;
            }
            Some(self.state.clone())
        } else {
            None
        }
    }
}
