use conrod::widget::{self, Widget};
use conrod::{utils, Sizeable, Positionable, Labelable, Color};

#[derive(WidgetCommon_)]
pub struct Sweeper<'a> {
    #[conrod(common_builder)]
    common: widget::CommonBuilder,
    label: &'a str,
    plotter: Plotter<'a>,
}

widget_ids! {
    struct Ids {
        title,
        canvas,
        plotter,
    }
}

pub struct State {
    ids: Ids,
}

impl<'a> Sweeper<'a> {
    pub fn new(label: &'a str, points: &'a [f64], current_point: usize, min_y: f64, max_y: f64) -> Self {
        Sweeper {
            common: widget::CommonBuilder::default(),
            label,
            plotter: Plotter::new(points, current_point, min_y, max_y,),
        }
    }
}

impl<'a> Widget for Sweeper<'a> {
    type State = State;
    type Style = ();
    type Event = ();

    fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
        State {
            ids: Ids::new(id_gen),
        }
    }

    fn style(&self) -> Self::Style {
        ()
    }

    fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
        let widget::UpdateArgs { ui, rect, state, id, .. } = args;
        let Sweeper { label, plotter, .. } = self;

        plotter
            .wh(rect.dim())
            .xy(rect.xy())
            // .place_on_kid_area(true)
            .parent(state.ids.canvas)
            .set(state.ids.plotter, ui);
        widget::TitleBar::new(label, id)
            .center_justify_label()
            .large_font(ui)
            .label_rgb(1.0, 1.0, 1.0)
            // .rgba(0.0, 0.0, 0.0, 0.0)
            .parent(state.ids.canvas)
            .set(state.ids.title, ui);
        widget::Rectangle::fill_with(rect.dim(), Color::Rgba(0.0, 0.0, 0.2, 1.0))
            .wh(rect.dim())
            .xy(rect.xy())
            .parent(id)
            .set(state.ids.canvas, ui);
    }
}

#[derive(WidgetCommon_)]
struct Plotter<'a> {
    #[conrod(common_builder)]
    common: widget::CommonBuilder,
    points: &'a [f64],
    current_point: usize,
    min_y: f64,
    max_y: f64,
}

widget_ids! {
    struct PlotterIds {
        point_path,
    }
}

struct PlotterState {
    ids: PlotterIds,
}

impl<'a> Plotter<'a> {
    fn new(points: &'a [f64], current_point: usize, min_y: f64, max_y: f64) -> Self {
        Plotter {
            common: widget::CommonBuilder::default(),
            points,
            current_point,
            min_y,
            max_y,
        }
    }
}

impl<'a> Widget for Plotter<'a> {
    type State = PlotterState;
    type Style = ();
    type Event = ();

    fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
        PlotterState {
            ids: PlotterIds::new(id_gen),
        }
    }

    fn style(&self) -> Self::Style {
        ()
    }

    fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
        let widget::UpdateArgs { ui, rect, state, id, .. } = args;
        let Plotter { points, current_point, min_y, max_y, .. } = self;

        let points_len = points.len();

        let x_to_scalar = |x| utils::map_range(x, 0, points_len, rect.left(), rect.right());
        let y_to_scalar = |y| utils::map_range(y, min_y, max_y, rect.bottom(), rect.top());

        let (before_current, after_current) = points.split_at(current_point);

        let translated_points = after_current.iter().chain(before_current).enumerate().map(|(x, y)| [x_to_scalar(x), y_to_scalar(*y)]);
        widget::PointPath::new(translated_points)
            .wh(rect.dim())
            .xy(rect.xy())
            .thickness(2.0)
            .parent(id)
            .graphics_for(id)
            .set(state.ids.point_path, ui);
    }
}
